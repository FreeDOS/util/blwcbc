# BLW-CBC

DOS Blowfish Cipher Program, a T-Pascal 7.0 implementation of the Blowfish cipher algorithm as described in Dr Dobb's, April 1994. Thanks go to Bruce Schneier, author of the algorithm.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## BLWCBC.LSM

<table>
<tr><td>title</td><td>BLW-CBC</td></tr>
<tr><td>version</td><td>15c</td></tr>
<tr><td>entered&nbsp;date</td><td>2019-01-20</td></tr>
<tr><td>description</td><td>Blowfish Cipher Program</td></tr>
<tr><td>summary</td><td>DOS Blowfish Cipher Program, a T-Pascal 7.0 implementation of the Blowfish cipher algorithm as described in Dr Dobb's, April 1994. Thanks go to Bruce Schneier, author of the algorithm.</td></tr>
<tr><td>keywords</td><td>dos blowfish cipher encryption</td></tr>
<tr><td>author</td><td>Dutra de Lacerda</td></tr>
<tr><td>maintained&nbsp;by</td><td>Dutra de Lacerda</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Public Domain</td></tr>
</table>
